# fedora-ssr-endpoint
A custom WordPress plugin for Fedora's server-side rendering

## Installation ##

1. Run `zip -r /tmp/fedora-ssr-endpoint-v1.4.0.zip fedora-ssr-endpoint`.
2. Go to `Plugins` → `Add New` on WordPress and upload the zip file.
3. Activate the plugin.\*

\* It may take a few minutes for the REST API endpoint to show up.
