<?php // vim:set ts=3 sw=3:
/**
 * Fedora SSR Endpoint
 *
 * @since               1.0.0
 * @package             fedora-ssr-endpoint
 *
 * @wordpress-plugin
 * Plugin Name:         Fedora SSR Endpoint
 * Plugin URI:          https://gitlab.com/fedora/websites-apps/themes/fedora-wordpress-plugin-ssr-endpoint
 * Description:         A plugin that provides a REST API endpoint for use by Fedora's server-side rendering
 * Version:             1.4.0
 * Author:              Gregory Lee Bartholomew
 * Author URI:          https://fedoraproject.org/wiki/User:Glb
 * License:             GPL-2.0+
 * License URI:         https://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:         fedora-ssr-endpoint
 * Derived From:        "Headless WordPress: The Ups And Downs Of Creating A Decoupled WordPress" by Denis Žoljom
 */

namespace Fedora_SSR_Endpoint;

if (!defined('ABSPATH')) {
	exit;
}

define('PERPAGE', 9);

class FpEndpoint {
	const PLUGIN_NAME = 'fedora-ssr-endpoint';

	public function fp_register() {
		register_rest_route(
			self::PLUGIN_NAME . '/v1', '/index/(?P<page>[\d]+)(?P<dir>[+-])?(?P<key>date|title|relevance)?(?P<terms>[\w\.\-\+]{3,300})?',
			array(
				'methods' => 'GET',
				'callback' => [ $this, 'fp_index' ],
				'permission_callback' => '__return_true',
			),
		);
		register_rest_route(
			self::PLUGIN_NAME . '/v1', '/post/(?P<slug>[^/]+)',
			array(
				'methods' => 'GET',
				'callback' => [ $this, 'fp_post' ],
				'permission_callback' => '__return_true',
			),
		);
	}

	public function fp_index($data) {
		$query = array(
			'post_type' => 'post',
			'post_status' => 'publish',
			'perm' => 'readable',
			'posts_per_page' => PERPAGE,
			'paged' => ($data['page'] === '0') ? '1' : $data['page'],
			'order' => ($data['dir'] === '+') ? 'ASC' : 'DESC',
			'orderby' => (isset($data['key'])) ? $data['key'] : 'date',
		);

		if (isset($data['terms'])) {
			$query += [
				's' => $data['terms'],
			];
		}

		$q = new \WP_Query($query);

		$i = 0;
		$posts = [];
		while ($q->have_posts()) {
			$post = &$posts[$i++];

			$q->the_post();
			$p = get_post();

			$post['slug'] = $p->post_name;
			$post['title'] = $p->post_title;
			$post['date'] = $p->post_date;
			if (class_exists('\WPDiscourse\Utilities\Utilities')) {
				$post['comments'] = (string)get_comments_number($p->ID);
			} else {
				$post['comments'] = $p->comment_count;
			}

			$feature = get_post_thumbnail_id();
			if ($feature) {
				$post['feature'] = wp_get_attachment_image_src(
					$feature,
					'post-image-thumbnail',
				)[0];
			}
		}

		$r = rest_ensure_response($posts);

		$r->header('X-WP-Total', $q->found_posts);
		$r->header('X-WP-TotalPages', $q->max_num_pages);

		wp_reset_postdata();

		return $r;
	}

	public function fp_post($data) {
		$q = new \WP_Query(array(
			'post_type' => 'post',
			'post_status' => 'publish',
			'perm' => 'readable',
			'name' => $data['slug'],
		));

		$post = [];
		if ($q->have_posts()) {
			$q->the_post();
			$p = get_post();

			$post['slug'] = $p->post_name;
			$post['title'] = $p->post_title;
			$post['date'] = $p->post_date;
			$post['content'] = $p->post_content;

			$authors = array();
			if (function_exists('cap_get_coauthor_terms_for_post')) {
				$t = cap_get_coauthor_terms_for_post($p->ID);
				while($author = array_shift($t)) {
					$u = get_user_by('login', $author->name);
					if ($u) {
						array_push($authors, [
							'username' => $author->name,
							'name' => $u->get('display_name'),
							'description' => $u->get('description'),
						]);
					}
				}
			} else {
				$u = get_user_by('id', $p->post_author);
				if ($u) {
					array_push($authors, [
						'username' => $u->get('user_login'),
						'name' => $u->get('display_name'),
						'description' => $u->get('description'),
					]);
				}
			}
			$post['authors'] = $authors;

			$feature = get_post_thumbnail_id();
			if ($feature) {
				$post['feature'] = wp_get_attachment_image_src(
					$feature,
					'full',
				)[0];
				$post['feature_caption'] = get_the_post_thumbnail_caption();
			}

			$comments = array();
			$use_internal_errors = libxml_use_internal_errors(true);
			$dom = new \DOMDocument('1.0', 'utf-8');
			if (class_exists('\WPDiscourse\Utilities\Utilities')) {
				$contentType = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
				$dom->loadHTML(
					$contentType . \WPDiscourse\Utilities\Utilities::get_discourse_comments($p->ID, false, false, null)
				);
			}
			if ($dom->childElementCount > 0) {
				foreach($dom->getElementsByTagName('li') as $li) {
					$fn = null;
					foreach($li->getElementsByTagName('span') as $s) {
						if (preg_match('/\bfn\b/', $s->className)) {
							$fc = $s->firstChild;
							$fn = ($fc->nodeType == XML_ELEMENT_NODE && $fc->tagName == 'a') ? $fc->nodeValue : $s->nodeValue;
						}
					}
					if ($fn === null) continue;
					$t = null;
					foreach($li->getElementsByTagName('div') as $d) {
						if (!preg_match('/\bcomment-content\b/', $d->className)) continue;
						libxml_clear_errors();
						libxml_use_internal_errors($use_internal_errors);
						$t = $dom->saveHTML($d);
						break;
					}
					if ($t === null) continue;
					array_push($comments, [
						'author' => $fn,
						'text' => $t,
					]);
				}
			} else {
				$cq = new \WP_Comment_Query(array(
					'post_id' => $p->ID,
					'type' => 'comment',
					'status' => 'approve',
					'hierarchical' => 'flat',
					'order' => 'ASC',
				));
				foreach($cq->get_comments() as $c) {
					array_push($comments, [
						'id' => $c->comment_ID,
						'to' => $c->comment_parent,
						'date' => $c->comment_date_gmt,
						'author' => $c->comment_author,
						'author_email' => $c->comment_author_email,
						'text' => $c->comment_content,
					]);
				}
			}
			$post['comments'] = $comments;
		}

		$r = rest_ensure_response($post);

		wp_reset_postdata();

		return $r;
	}
}

$fp_endpoint = new FpEndpoint();

add_action('rest_api_init', [ $fp_endpoint, 'fp_register' ]);
?>
